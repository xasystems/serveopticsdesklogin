<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : template
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>
<%@page import="org.apache.logging.log4j.LogManager"%>
<%@page import="org.apache.logging.log4j.Logger"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.LinkedHashMap"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%
    if(session.getAttribute("authenticated")==null)
    {
       response.sendRedirect("index.jsp");
    }
    else
    {
        if(!session.getAttribute("administration").toString().equalsIgnoreCase("true"))
        {
            response.sendRedirect("no_permission.jsp");
        }
        else
        {
            Logger logger = LogManager.getLogger();
        
            
    %>
    <jsp:include page='header.jsp'>
        <jsp:param name="page_type" value=""/>
    </jsp:include>	

    <jsp:include page='menu_analytics.jsp'>
        <jsp:param name="active_menu" value=""/>
    </jsp:include>
    <!-- BEGIN PAGE LEVEL CSS-->
    <!-- END PAGE LEVEL CSS-->
    
    <div class="app-content content">
        <div class="content-wrapper">
            <!-- start content here-->
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title">Administration</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home_analytics.jsp">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Administration</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div> <!--End breadcrumbs -->
            <div class="row match-height">
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">License</h4>
                                <p class="card-text">View/Manage the ServeOptics license.</p>
                                <a href="admin_license.jsp" class="btn btn-outline-blue">Manage Licenses</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Users</h4>
                                <p class="card-text">View/Manage the Users.</p>
                                <a href="admin_users.jsp" class="btn btn-outline-blue">Manage Users</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Roles</h4>
                                <p class="card-text">View/Manage User Roles</p>
                                <a href="admin_roles.jsp" class="btn btn-outline-blue">Manage Roles</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">System Schedules</h4>
                                <p class="card-text">View/Manage System Schedules</p>
                                <a href="admin_system_schedules.jsp" class="btn btn-outline-blue">Manage System Schedules</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row match-height">
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Email Settings</h4>
                                <p class="card-text">View/Manage Email Settings.</p>
                                <a href="admin_email.jsp" class="btn btn-outline-blue">Manage Email</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">ServeOptics URL</h4>
                                <p class="card-text">Enter the ServeOptics URL.</p>
                                <a href="admin_url.jsp" class="btn btn-outline-blue">Enter URL</a>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Category/Subcategory Options</h4>
                                <p class="card-text">Add/Edit/Delete Incident Categories/Subcategories Options </p>
                                <a href="admin_category_subcategory.jsp" class="btn btn-outline-blue">Change Options</a>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title">Form Select Options</h4>
                                <p class="card-text">Add/Edit/Delete Form Select Options <br><em>Such as Priority, State, etc..</em></p>
                                <a href="admin_system_select_fields.jsp" class="btn btn-outline-blue">Change Options</a>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            
            <!-- end content here-->
        </div>        
    </div>
    <jsp:include page='footer.jsp'>
        <jsp:param name="parameter" value=""/>
    </jsp:include>	
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->   
    <%
        } //end if not admin permission
    }//end if not redirected
    %>
    </body>
</html>
