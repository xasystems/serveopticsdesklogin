<!--Copyright 2020 Real Data Technologies, Inc. , All rights reserved.-->
<%-- 
    Document   : index
    Created on : Dec 11, 2018, 8:03:00 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%

String error = "";
boolean license_error = false;
boolean password_error = false;
try
{
    error = request.getParameter("error"); //License Limit reached!   or   Username/Password incorrect!
    //System.out.println("Error=" + error + "=");
    if(error != null)
    {
        if(error.equalsIgnoreCase("License"))
        {
            license_error = true;
        }
        if(error.equalsIgnoreCase("Password"))
        {
            password_error = true;
        }
    }
}
catch(Exception e)
{
    error = "";
    System.out.println("Exception=" + e + "=");
}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Login Page - ServeOptics Desk</title>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Basic Css files -->
        <link href="assets/css/app_theme.css" rel="stylesheet" type="text/css">

    </head>


    <body>

       
        <div class="global-container loginPage">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="login-img">
                            <img src="assets/images/login-page-bg.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 rightContent">
                        <div class="card login-form">
                            <div class="card-body">
                                <a href="#" style="width: 150px; display: block; margin: auto auto 30px;"><img src="assets/images/serveoptics-logo.png" style="max-width: 100%;" alt=""></a>
                                <h1 class="boldFont xlarge-font mb-0">Login to continue</h1>
                                <div class="card-text">
                                    
                                    <form method="post" action="login">
                                        <!-- to error: add class "has-danger" -->
                                        <%
                                        if(license_error)
                                        {
                                            %>
                                            <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                                <strong>License has expired or login limit reached!</strong> Contact your administrator.
                                            </div>
                                            <%
                                        }
                                        if(password_error)
                                        {
                                            %>
                                            <div class="alert alert-warning alert-dismissible mb-2" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                                <strong>Warning!</strong> Username/Password is incorrect!</a>.
                                            </div>
                                            <% 
                                        }
                                        %>
                                        <div class="form-group">
                                            <label class="formLabel mb-0" for="exampleInputEmail1">Email address</label>
                                            <div class="formField clr md mb-15 whtbg">
                                                <input type="email" value="" class="no-border" placeholder="" name="username">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="formLabel mb-0" for="exampleInputPassword1">Password</label>
                                            <a href="#" style="float:right;font-size:12px;">Forgot password?</a>
                                            <div class="formField clr md mb-15 whtbg">
                                                <input type="password" value="" class="no-border" placeholder="" name="password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <fieldset>
                                                    <input type="checkbox" id="remember-me" class="chk-remember">
                                                    <label for="remember-me"> Remember Me</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12 ">
                                                <fieldset>
                                                    <label for="home_selection"> Dashboard:</label>
                                                    <input type="radio" name="home_selection" value="analytics">&nbsp;Analytics
                                                    &nbsp;&nbsp;
                                                    <input checked type="radio" name="home_selection" value="service_desk">&nbsp;Service&nbsp;Desk
                                                </fieldset>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary-new btn-block">Sign in</button>
                                        
                                        <div class="sign-up pt-5">
                                            Don't have an account? <a href="#">Create One</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <!-- App js -->
        <script src="assets/js/app.js"></script>

    </body>
</html>