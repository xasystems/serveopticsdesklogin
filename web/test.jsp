<!--Copyright 2020 XaSystems, Inc. , All rights reserved.-->
<%-- 
    Document   : incident_survey
    Created on : Dec 10, 2018, 6:13:59 PM
    Author     : server-xc6701
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.StringReader"%>
<%@ page import="javax.json.Json"%>
<%@ page import="javax.json.JsonArray"%>
<%@ page import="javax.json.JsonArrayBuilder"%>
<%@ page import="javax.json.JsonObjectBuilder"%>
<%@ page import="javax.json.JsonObject"%>
<%@ page import="javax.json.JsonReader"%>

<!DOCTYPE html>
<%
    

            String context_dir = request.getServletContext().getRealPath("");
            LinkedHashMap props = support.config.get_config(context_dir);
            Connection con = db.db_util.get_connection(context_dir, props);
            SimpleDateFormat filter_format = new SimpleDateFormat("MM/dd/yyyy hh:mm a"); //01/19/2019 7:30 PM
            SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddHHmmss");
            //SimpleDateFormat filter_format2 = new SimpleDateFormat("yyyy"); //01/12/2019 12:00 AM
            java.util.Date filter_start = new java.util.Date();
            java.util.Date filter_end = new java.util.Date();
            String start = "";
            String end = "";
            String filter_by_type = "";
            String filter_by_name = "";
            String date_range = "";
            String selected = "";
            String all_companies[] = db.get_incidents.distinct_company(con);
            String all_departments[] = db.get_incidents.distinct_department(con);
            String all_sites[] = db.get_incidents.distinct_site(con);
            String all_locations[] = db.get_incidents.distinct_location(con);

            JsonArrayBuilder company_builder = Json.createArrayBuilder();

            for (int a = 0; a < all_companies.length; a++) {
                JsonObjectBuilder company_object = Json.createObjectBuilder();
                company_object.add("label", all_companies[a]);
                company_object.add("value", all_companies[a]);
                //company_object.build();
                company_builder.add(company_object);
                //System.out.println("company_object=" + company_object.toString());
            }
            JsonArray json_companies = company_builder.build();

            System.out.println("json_companies=" + json_companies.toString());
            try {
                date_range = request.getParameter("date_range");
                if (date_range.equalsIgnoreCase("null") || date_range == null) {
                    date_range = support.filter_dates.past_30_days();
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                date_range = support.filter_dates.past_30_days();
            }
            try {
                filter_by_type = request.getParameter("filter_by_type");
                if (filter_by_type.equalsIgnoreCase("null") || filter_by_type == null) {
                    filter_by_type = "all";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                filter_by_type = "all";
            }
            try {
                filter_by_name = request.getParameter("filter_by_name");
                if (filter_by_name.equalsIgnoreCase("null") || filter_by_name == null) {
                    filter_by_name = "all";
                }
            } catch (Exception e) {
                //if not set then default to past 30 days
                filter_by_name = "all";
            }
            try {
                //01/12/2019 12:00 AM - 01/12/2019 11:59 PM&group_id=all
                //System.out.println("date_range=" + date_range);
                String temp[] = date_range.split("-");
                filter_start = filter_format.parse(temp[0]);
                //filter_start = filter_format.parse("01/12/2019 12:00 AM");

                filter_end = filter_format.parse(temp[1]);
                //System.out.println("filter_end=" + temp[1].trim() + " ====" + filter_end);

                start = timestamp.format(filter_start);
                end = timestamp.format(filter_end);
            } catch (Exception e) {
                System.out.println("Exception in incident_service_compliance.jsp:=" + e);
            }
            DecimalFormat two_decimals = new DecimalFormat("0.00");
            DecimalFormat no_decimals = new DecimalFormat("#");

%>
<jsp:include page="header.jsp">
    <jsp:param name="page_type" value=""/>
</jsp:include>	

<jsp:include page="menu_service_desk.jsp">
    <jsp:param name="active_menu" value=""/>
</jsp:include>
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
<script src="assets/js/highcharts/highcharts.js"></script>
<script src="assets/js/highcharts/data.js"></script>
<script src="assets/js/highcharts/drilldown.js"></script>
<script src="assets/js/highcharts/exporting.js"></script>
<!-- END VENDOR CSS-->
<!--<input type="hidden" name="json_companies" id="json_companies" value="<%=json_companies%>"/>-->
<div class="app-content content">
    <div class="content-wrapper">
        <!-- start content here-->
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title"><i class="ft ft-bar-chart"></i>&nbsp;Incident Service Compliance</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"> 
                                <a href="#" onClick="parent.location = '<%=session.getAttribute("sla_dashboard_referer")%>'"><i class="la la-angle-left"></i>Back</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="form-group">
                    <label>Date Range</label>
                    <div class="input-group">
                        <input type="text" id="filter_date_range" name="filter_date_range" value="<%=date_range%>" class="form-control datetime" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-2 col-4">
                <div class="form-group">
                    <label>Filter by</label>
                    <div class="input-group">
                        <select onchange="update_filter_by_name()" class="select2-placeholder form-control" name="filter_by_type" id="filter_by_type">
                            <%
                                String values[] = {"all", "company", "department", "site", "location"};
                                String labels[] = {"All", "Company", "Department", "Site", "Location"};
                                for (int a = 0; a < values.length; a++) {
                                    selected = "";
                                    if (values[a].equalsIgnoreCase(filter_by_type)) {
                                        selected = "SELECTED";
                                    }
                            %>
                            <option <%=selected%> value="<%=values[a]%>"><%=labels[a]%></option>
                            <%
                                }
                            %>
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-filter"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-4">
                <div class="form-group">
                    <label>Name</label>
                    <div class="input-group">
                        <select class="select2-placeholder form-control" name="filter_by_name" id="filter_by_name">                                
                            <%
                                if (filter_by_name.equalsIgnoreCase("all")) {
                            %>
                            <option SELECTED value="all">All</option>
                            <%
                            } else {
                            %>
                            <option value="all">All</option>
                            <%
                                }
                                if (filter_by_type.equalsIgnoreCase("company")) {
                                    for (int a = 0; a < all_companies.length; a++) {
                                        selected = "";
                                        if (all_companies[a].equalsIgnoreCase(filter_by_name)) {
                                            selected = "SELECTED";
                                        }
                            %>
                            <option <%=selected%> value="<%=all_companies[a]%>"><%=all_companies[a]%></option>
                            <%
                                }
                            } else {
                                if (filter_by_type.equalsIgnoreCase("department")) {
                                    for (int a = 0; a < all_departments.length; a++) {
                                        selected = "";
                                        if (all_departments[a].equalsIgnoreCase(filter_by_name)) {
                                            selected = "SELECTED";
                                        }
                            %>
                            <option <%=selected%> value="<%=all_departments[a]%>"><%=all_departments[a]%></option>
                            <%
                                }
                            } else {
                                if (filter_by_type.equalsIgnoreCase("site")) {
                                    for (int a = 0; a < all_sites.length; a++) {
                                        selected = "";
                                        if (all_sites[a].equalsIgnoreCase(filter_by_name)) {
                                            selected = "SELECTED";
                                        }
                            %>
                            <option <%=selected%> value="<%=all_sites[a]%>"><%=all_sites[a]%></option>
                            <%
                                }
                            } else {
                                if (filter_by_type.equalsIgnoreCase("location")) {
                                    for (int a = 0; a < all_locations.length; a++) {
                                        selected = "";
                                        if (all_locations[a].equalsIgnoreCase(filter_by_name)) {
                                            selected = "SELECTED";
                                        }
                            %>
                            <option <%=selected%> value="<%=all_locations[a]%>"><%=all_locations[a]%></option>
                            <%
                                                }
                                            }
                                        }
                                    }
                                }
                            %>
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <span class="la la-filter"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-1 col-2">
                <label>&nbsp;</label>
                <div class="form-actions">
                    <button type="button" onclick="reload_page()" class="btn btn-blue mr-1">
                        <i class="fa-filter"></i> Apply
                    </button>
                </div>
            </div>
        </div>




        <!-- end content here-->
    </div>        
</div>
<jsp:include page="footer.jsp">
    <jsp:param name="parameter" value=""/>
</jsp:include>	
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>

<script src="app-assets/js/scripts/popover/popover.js"></script>
<script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>

<script>
                    function reload_page()
                    {
                        var filter_by_type = document.getElementById("filter_by_type").value;
                        var filter_by_name = document.getElementById("filter_by_name").value;
                        var filter_date_range = document.getElementById("filter_date_range").value;
                        var URL = "incident_service_compliance.jsp?date_range=" + filter_date_range + "&filter_by_type=" + filter_by_type + "&filter_by_name=" + filter_by_name;
                        window.location.href = URL;
                    }
                    function update_filter_by_name()
                    {
                        //clear the existing choices
                        document.getElementById("filter_by_name").options.length = 0;
                        var filter_by_type = document.getElementById("filter_by_type").value;
                        
                        let dropdown = document.getElementById("filter_by_name");
                        if (filter_by_type === "company")
                        {
                            var text = '[{ "name":"John", "age":"30", "city":"New York"}]';
                            var obj = JSON.parse(text);
                            alert("obj length=" + obj.length);
                            alert(obj.age);
                            
                            
                            
                            
                            const data = JSON.parse('[{ "label":"John", "value":30}]');
                            let option;
                            alert("data length=" + data.length);
                            option = document.createElement("option");
                            option.text = "All";
                            option.value = "All";
                            dropdown.add(option);
                            for (let i = 0; i < data.length; i++)
                            {
                                option = document.createElement("option");
                                option.text = data[i].label;
                                option.value = data[i].value;
                                dropdown.add(option);
                            }
                        }
                    }


</script>








</html>