/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ralph
 */
public class get_active_instances extends HttpServlet 
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String active_instances = "";
        String encrypted_active_instances = "";
        Connection con;
        try
        {
            con = db.db_util.get_connection(context_dir,props);
            String get_instances = "SELECT * FROM contracts WHERE active='true'";
            PreparedStatement stmt = con.prepareStatement(get_instances);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                if(active_instances.equalsIgnoreCase(""))
                {
                    active_instances = rs.getString("id");
                }
                else
                {
                    active_instances = active_instances + "," + rs.getString("id");
                }
            }
            encrypted_active_instances = support.encrypt_utils.encrypt(context_dir, active_instances);
            stmt.close();
            con.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception is get_active_instances=" + e);
        }
         
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter writer = response.getWriter()) 
        {
            response.setStatus(200);
            response.setContentType("text/xml");
            String stringToSend = URLEncoder.encode(encrypted_active_instances.toString(), "UTF-8");
            writer.append("data=" + stringToSend);
            writer.flush();            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
