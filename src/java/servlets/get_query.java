/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ralph
 */
public class get_query extends HttpServlet 
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String shared_key = props.get("shared_key").toString().trim();
        //System.out.println("shared_key=" + shared_key);
        String shared_user = props.get("shared_user").toString().trim();
        //System.out.println("shared_user=" + shared_user);
        String user = "";
        String key = "";
        String query_type = "";
        String query = "";
        String requested_fields = "";
        String records = "";
        boolean authorized = false;
        String in_data = "";
        try 
        {
            String request_parameter = request.getParameter("request");
            //System.out.println("get_query.processRequest request_parameter=" + request_parameter);
            
            byte[] decodedBytes = Base64.getDecoder().decode(request_parameter);
            String decoded_encrypted_request = new String(decodedBytes);
            
            
            //System.out.println("DECODED get_query.processRequest request_parameter=" + decoded_encrypted_request);
            
            String decrypted_parameter = support.encrypt_utils.decrypt(context_dir, decoded_encrypted_request);
            //System.out.println("DECRYPTED get_query.processRequest request_parameter=" + decrypted_parameter);
            
            String temp[] = decrypted_parameter.split("&");
            for(int a = 0; a < temp.length; a++)
            {
                //System.out.println("looking at=" + temp[a]);
                if(temp[a].startsWith("user="))
                {
                    user = temp[a].replace("user=", "");
                }
                else if(temp[a].startsWith("key="))
                {
                    key = temp[a].replace("key=", "");
                }
                else if(temp[a].startsWith("query_type="))
                {
                    query_type = temp[a].replace("query_type=", "");
                }
                else if(temp[a].startsWith("query="))
                {
                    query = temp[a].replace("query=", "");
                }
                else if(temp[a].startsWith("requested_fields="))
                {
                    requested_fields = temp[a].replace("requested_fields=", "");
                }
            }    
            //System.out.println("user=" + user);            
            //System.out.println("key=" + key);
            //System.out.println("query_type=" + query_type);
            //System.out.println("query=" + query);
            //System.out.println("fields=" + requested_fields);
            try
            {
                if(user.equalsIgnoreCase(shared_user) && key.equalsIgnoreCase(shared_key))
                {
                    authorized = true;
                }                
            }
            catch(Exception e)
            {
                authorized = false;
                System.out.println("Exception is get_query authorization=" + e);
            }
            //System.out.println("authorized=" + authorized);
            
            if(authorized)
            {
                //System.out.println("query_type=" + query_type + " query=" + query);
                Connection con = db.db_util.get_connection(context_dir,props);
                try
                {               
                    PreparedStatement stmt = con.prepareStatement(query);
                    if(query_type.equalsIgnoreCase("POST"))
                    {
                        
                        //System.out.println("query_type is POST");
                        int rows_affected = stmt.executeUpdate();
                        if(rows_affected == 0)
                        {
                            records = "{\"success\": \"false\"}";
                        }
                        else
                        {
                            records = "{\"success\": \"true\"}";
                        }
                    }
                    else if(query_type.equalsIgnoreCase("GET"))
                    {
                        //System.out.println("query_type is GET");
                        String fields[] = requested_fields.split(",");
                        //SELECT * FROM `management1`.`users`;

                        ResultSet rs = stmt.executeQuery(query);
                        //System.out.println("stmt=" + query);
                        boolean is_first_record = true;
                        records = "{\"records\": [";
                        while(rs.next())
                        {
                            //System.out.println("got a record");
                            String record = "";
                            if(is_first_record)
                            {
                                record = "{";
                            }
                            else
                            {
                                record = ",{";
                            }  
                            boolean first_field = true;
                            for (String field_name : fields) 
                            {
                                String field_value = rs.getString(field_name);
                                //System.out.println("field_name=" + field_name + " field_value=" + field_value);
                                if(first_field)
                                {
                                    record = record + "\"" + field_name + "\":" + "\"" + field_value + "\"";
                                }
                                else
                                {
                                    record = record + ",\"" + field_name + "\":" + "\"" + field_value + "\"";
                                }
                                first_field = false;
                            }
                            record = record + "}"; //close the record
                            is_first_record = false;
                            records = records + record;
                        }
                        //complete the JSON records
                        records = records + "]}";
                    }
                    else
                    {
                        //System.out.println("Unknown query_type=" + query_type);
                    }
                    stmt.close();
                    con.close();
                }
                catch(Exception e)
                {
                    System.out.println("Exception in get_query mysql:=" + e);
                }
            }//end if authorized
            
            
            String encrypted_records = support.encrypt_utils.encrypt(context_dir, records);
            //System.out.println("encrypted_rec=" + encrypted_records);            
            String encodedString = Base64.getEncoder().encodeToString(encrypted_records.getBytes());
            //System.out.println("encodedString=" + encodedString);  
            
            decodedBytes = Base64.getDecoder().decode(encodedString);
            String decodedString = new String(decodedBytes);
            //System.out.println("decodedString=" + decodedString);  
            //System.out.println("---------------------------------------------------------------------------");
            String encoded_encrypted_records = Base64.getEncoder().encodeToString(encrypted_records.getBytes());
            
            //String decoded_encrypted_records = Base64.getDecoder().decode(encoded_encrypted_records).toString();
            //System.out.println("encrypted_records=" + new String(decoded_encrypted_records));
            
            byte[] decoded_encrypted_records = Base64.getDecoder().decode(encoded_encrypted_records);
            String decodedString1 = new String(decoded_encrypted_records);
            //System.out.println("encrypted_records=" + decodedString1);
            
            
            
            response.setStatus(HttpServletResponse.SC_OK); 
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            //System.out.println("sending=" + encoded_encrypted_records);
            out.println(encoded_encrypted_records);
            out.close();
            
        }
        catch(Exception e)
        {
            System.out.println("Exception in get_query servlet=" + e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().print("Server side database issue");
            response.getWriter().close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
