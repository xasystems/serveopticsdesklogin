/*
//Copyright 2021 XaSystems, Inc. , All rights reserved.
 */
package servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ralph
 */
public class login extends HttpServlet {

    private static Logger logger = LogManager.getLogger();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        boolean login_success = true;
        HttpSession session = request.getSession();
        logger.debug("DEBUG: Start login servlet!:");
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        
        //get parameters
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String home_selection = request.getParameter("home_selection");
        
        try
        {
            //get a connection
            Connection con = db.db_util.get_connection(context_dir, props);
            //get user info
            HashMap user = db.get_users.by_email(con, username);
            //check for null as user
            if(user == null)
            {
                login_success = false;
                String RedirectURL = "index.jsp" ;
                session.setAttribute("error", "Unknown user");
                response.sendRedirect(RedirectURL + "?error=Password");
            }
            else
            {
                String contract_id = user.get("contract_id").toString();
                //System.out.println("contract_number=" + contract_number);
                
                HashMap contract = db.get_contract.by_id(con, contract_id);
                String license = contract.get("license").toString();
                String url = contract.get("url").toString();
                String active = contract.get("active").toString().trim();
                String name = contract.get("name").toString();
                
                //System.out.println("url=" + url);
                //System.out.println("license=" + license);
                //check license is not expired
                boolean license_good = support.license.expired(context_dir, license);
                //System.out.println("license_good=" + license_good);
                //if license has not expired
                if(license_good && active.equalsIgnoreCase("true"))
                {
                    //everything is good, send them to the assigned url
                    //create an encrypted string with username:password:contract_id
                    //contract_id=db instance name
                    String p = support.encrypt_utils.encrypt(context_dir, username + "sEp,sEp" + password + "sEp,sEp" + home_selection + "sEp,sEp" + contract_id + "sEp,sEp" + license + "sEp,sEp" + name);
                    String RedirectURL =  url + "?q=" + URLEncoder.encode(p, "UTF-8");
                    response.sendRedirect(RedirectURL);
                }
                else
                {
                    String RedirectURL = "index.jsp" ;
                    session.setAttribute("error", "Contract has expired/or inactive. Contact support to renew your license");
                    response.sendRedirect(RedirectURL + "?error=License");
                }
            }
            con.close();
        }
        catch(Exception e)
        {
            System.out.println("Exception in login servlet=" + e);
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
