/*
 * Copyright 2021 Real Data Technologies, Inc. , All rights reserved.
 * 
 * 
 */
package servlets;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ralph
 */
public class user_update extends HttpServlet 
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        String context_dir = request.getServletContext().getRealPath("");
        LinkedHashMap props = support.config.get_config(context_dir);
        String shared_key = props.get("shared_key").toString().trim();
        String shared_user = props.get("shared_user").toString().trim();
        boolean authorized = false;
        String in_data = "";
        try 
        {
            int len = request.getContentLength();
            byte[] input = new byte[len];
        
            ServletInputStream sin = request.getInputStream();
            int c, count = 0 ;
            while ((c = sin.read(input, count, input.length-count)) != -1) 
            {
                count +=c;
            }
            sin.close();
        
            String inString = new String(input);
            int index = inString.indexOf("=");
            if (index == -1) 
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print("SC_BAD_REQUEST");
                response.getWriter().close();
                return;
            }
            String value = inString.substring(index + 1);
            //decode application/x-www-form-urlencoded string
            String decodedString = URLDecoder.decode(value, "UTF-8");
            
            // set the response code and write the response data
            response.setStatus(HttpServletResponse.SC_OK);
            
            //Process the received data
            if(decodedString.startsWith("data="))
            {
                in_data = decodedString.substring(5,decodedString.length());
            }
            else
            {
                in_data = decodedString;
            }
            //parse the JSON
            JsonReader reader = Json.createReader(new StringReader(in_data));
            JsonObject resultObject = reader.readObject();
            String authorization = resultObject.getString("authorization");
            String request_time = resultObject.getString("request_time");
            try
            {
                String decrypted_auth = support.encrypt_utils.decrypt(context_dir, authorization);
                String temp[] = decrypted_auth.split("sEp,sEp");
                String auth_shared_user = temp[0];
                String auth_shared_key = temp[1];
                String auth_timestamp = temp[2];
                if(auth_shared_user.equalsIgnoreCase(shared_user) && auth_shared_key.equalsIgnoreCase(shared_key) && auth_timestamp.equalsIgnoreCase(request_time))
                {
                    authorized = true;
                }
                
            }
            catch(Exception e)
            {
                authorized = false;
                System.out.println("Exception is user_update authorization=" + e);
            }
            
            if(authorized)
            {
                //System.out.println("got authorization=" + authorization);
                //System.out.println("got request_time=" + request_time);

                JsonArray users = resultObject.getJsonArray("users");

                users = support.encrypt_utils.decrypt_jsonarray(context_dir, users);
                
                Connection con = db.db_util.get_connection(context_dir,props);


                for(int a = 0; a < users.size();a++)
                {
                    JsonObject this_record = users.getJsonObject(a); //a single incident record
                    String action = this_record.getString("action");
                    //System.out.println("action=" + action);

                    String contract_id = this_record.getString("contract_id");
                    //System.out.println("contract_id=" + contract_id);

                    String id = this_record.getString("id");
                    //System.out.println("id=" + id);

                    String username = this_record.getString("username");
                    
                    String password = this_record.getString("password");
                    
                    String first = this_record.getString("first");
                    
                    String mi = this_record.getString("mi");
                    
                    String last = this_record.getString("last");
                    
                    String address_1 = this_record.getString("address_1");
                    
                    String address_2 = this_record.getString("address_2");
                    
                    String city = this_record.getString("city");
                    
                    String state = this_record.getString("state");
                    
                    String zip = this_record.getString("zip");
                    
                    String email = this_record.getString("email");
                    
                    String phone_office = this_record.getString("phone_office");
                    
                    String phone_mobile = this_record.getString("phone_mobile");
                    
                    String notes = this_record.getString("notes");
                    
                    if(action.equalsIgnoreCase("create"))
                    {
                        //replace into users
                        try
                        {               
                            String replace_user_table = "INSERT INTO users (id,contract_id,username,password,first,mi,last,address_1,address_2,city,state,zip,email,phone_office,phone_mobile,notes) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                            PreparedStatement stmt = con.prepareStatement(replace_user_table);
                            stmt.setString(1,id);//id
                            stmt.setString(2,contract_id); //id
                            stmt.setString(3,username); //name
                            stmt.setString(4,password);//description
                            stmt.setString(5,first);//owner_group_id
                            stmt.setString(6,mi);//mi
                            stmt.setString(7,last);//last 
                            stmt.setString(8,address_1);//address_1
                            stmt.setString(9,address_2);//address_2
                            stmt.setString(10,city);//city
                            stmt.setString(11,state);//state
                            stmt.setString(12,zip);//zip
                            stmt.setString(13,email);//email
                            stmt.setString(14,phone_office);//phone_office
                            stmt.setString(15,phone_mobile);//phone_mobile
                            stmt.setString(16,notes);//notes

                            stmt.execute();  
                        }
                        catch(Exception e)
                        {
                            System.out.println("Exception in user_update servlet create a new user:=" + e);
                        }

                    }
                    else if(action.equalsIgnoreCase("update"))
                    {
                        //UPDATE into users
                        try
                        {
                            String update_user_table = "REPLACE INTO users (id,contract_id,username,password,first,mi,last,address_1,address_2,city,state,zip,email,phone_office,phone_mobile,notes) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                            PreparedStatement stmt = con.prepareStatement(update_user_table);
                            stmt.setString(1,id);//id
                            stmt.setString(2,contract_id); //id
                            stmt.setString(3,username); //name
                            stmt.setString(4,password);//description
                            stmt.setString(5,first);//owner_group_id
                            stmt.setString(6,mi);//mi
                            stmt.setString(7,last);//last 
                            stmt.setString(8,address_1);//address_1
                            stmt.setString(9,address_2);//address_2
                            stmt.setString(10,city);//city
                            stmt.setString(11,state);//state
                            stmt.setString(12,zip);//zip
                            stmt.setString(13,email);//email
                            stmt.setString(14,phone_office);//phone_office
                            stmt.setString(15,phone_mobile);//phone_mobile
                            stmt.setString(16,notes);//notes
                            stmt.execute();  
                        }
                        catch(Exception e)
                        {
                            System.out.println("Exception in user_update servlet update a user:=" + e);
                        }

                    }
                    else if(action.equalsIgnoreCase("delete"))
                    {
                        //DELETE FROM users
                        String delete_user_table = "DELETE FROM users WHERE id=? AND contract_id=?";
                        PreparedStatement stmt = con.prepareStatement(delete_user_table);
                        stmt.setString(1,id); //id
                        stmt.setString(2,contract_id);//contract_id
                        stmt.execute(); 
                    }
                    
                }
                con.close();
            }//end if authorized
            response.setStatus(HttpServletResponse.SC_OK); 
        }
        catch(Exception e)
        {
            System.out.println("Exception in user_update servlet=" + e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().print("Server side database issue");
            response.getWriter().close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
