/*
 * Copyright 2019 Real Data Technologies, Inc. , All rights reserved.
 */
package support;

/**
 *
 * @author Ralph
 */
public class string_case_changer 
{
    public static String to_camel_case(String inputString) {
        String result = "";
        if (inputString.length() == 0) {
            return result;
        }
        char firstChar = inputString.charAt(0);
        char firstCharToUpperCase = Character.toUpperCase(firstChar);
        result = result + firstCharToUpperCase;
        for (int i = 1; i < inputString.length(); i++) {
            char currentChar = inputString.charAt(i);
            char previousChar = inputString.charAt(i - 1);
            if (previousChar == ' ') {
                char currentCharToUpperCase = Character.toUpperCase(currentChar);
                result = result + currentCharToUpperCase;
            } else {
                char currentCharToLowerCase = Character.toLowerCase(currentChar);
                result = result + currentCharToLowerCase;
            }
        }
        return result;
    }
    public static String sla_threshold_operator(String inputString) 
    {
        String db_values[] = {"greater_than","greater_than_or_equal","equal_to","not_equal_to","less_than_or_equal","less_than"};
        String display_values[] = {"Greater Than","Greater than or equal","Equal to","Not equal to","Less than or equal","Less than"};
        String result  = "";
        for(int a = 0; a < db_values.length;a++)
        {
            if(db_values[a].equalsIgnoreCase(inputString))
            {
                result = display_values[a];
                a = db_values.length;
            }
        }        
        return result;
    }
    public static String sla_threshold_units(String inputString) 
    {
        String db_values[] = {"seconds","minutes","hours","days"};
        String display_values[] = {"Seconds","Minutes","Hours","Days"};
        String result  = "";
        for(int a = 0; a < db_values.length;a++)
        {
            if(db_values[a].equalsIgnoreCase(inputString))
            {
                result = display_values[a];
                a = db_values.length;
            }
        }        
        return result;
    }
    public static String contact_sla_type(String inputString) 
    {
        String db_values[] = {"ABANDONED_CONTACT_RATE", "ABANDONED_CONTACT_COUNT", "AVERAGE_POST_PROCESS_TIME", "AVERAGE_SPEED_TO_ANSWER", "AVERAGE_HANDLE_TIME", "AVERAGE_PROCESSING_TIME"};
        String display_values[] = {"Abandoned Contact Rate", "Abandoned Contact Count", "Average Post Process Time", "Average Speed to Answer", "Average Handle Time", "Average Processing Time"};
        String result  = "";
        for(int a = 0; a < db_values.length;a++)
        {
            if(db_values[a].equalsIgnoreCase(inputString))
            {
                result = display_values[a];
                a = db_values.length;
            }
        }        
        return result;
    }  
    public static String request_state(String inputString) 
    {
        String db_values[] = {"pending_approval","approved","closed_complete","closed_incomplete","closed_cancelled","closed_Rejected"};
        String display_values[] = {"Pending Approval","Approved","Closed Complete","Closed Incomplete","Closed Cancelled","Closed Rejected"};
        String result  = "";
        for(int a = 0; a < db_values.length;a++)
        {
            if(db_values[a].equalsIgnoreCase(inputString))
            {
                result = display_values[a];
                a = db_values.length;
            }
        }        
        return result;
    }                           
}
