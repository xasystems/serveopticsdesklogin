//Copyright 2020 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package support;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author server-xc6701
 */
public class config 
{
    private static Logger logger = LogManager.getLogger();
    
    public static LinkedHashMap get_config(String context_dir)
    {
        boolean needs_file_rewrite = false;
        LinkedHashMap source_linked_has_map = new LinkedHashMap(); 
        //logger.debug("Start config.get_config");
        File config_file = new File(context_dir + "/WEB-INF/serveoptics.properties");
        
        try
        {
            if (config_file.exists()) 
            {
                //logger.debug("config.get_config  config_file found" + config_file.getAbsolutePath());
                source_linked_has_map = properties_util.read.properties_file(config_file);
                String[] env_variables = {
                    "MANAGEMENT_DB",
                    "MANAGEMENT_DB_HOST",
                    "MANAGEMENT_DB_PORT",
                    "MANAGEMENT_DB_USER",
                    "MANAGEMENT_DB_PASSWORD",
                    "MANAGEMENT_DB_PASSWORD_ENCRYPTED",
                    "LOGIN_URL",
                    "SERVICEDESK_URL"
                };
                String[] props_variables = {
                    "db",
                    "db_host",
                    "db_port",
                    "db_user",
                    "db_password",
                    "db_password_encrypted",
                    "login_url",
                    "servicedesk_url"
                };
                for (int i = 0; i < env_variables.length; i++)
                {
                    String env_variable_name = env_variables[i];
                    String env_variable_value = System.getenv(env_variable_name);
                    if (env_variable_value != null && source_linked_has_map.get(props_variables[i]) != null && env_variable_value != source_linked_has_map.get(props_variables[i]))
                    {
                        System.out.println("replacing server property " + env_variable_name + " by value from environment");
                        source_linked_has_map.replace(props_variables[i], env_variable_value);     
                    }
                }

                String db_password = source_linked_has_map.get("db_password").toString();        
                String db_password_encrypted = source_linked_has_map.get("db_password_encrypted").toString();
                String email_password = source_linked_has_map.get("email_password").toString();
                String email_password_encrypted = source_linked_has_map.get("email_password_encrypted").toString();        
                  
                if(db_password_encrypted.equalsIgnoreCase("false"))
                {
                    System.out.println("line 47");
                    needs_file_rewrite = true;
                    //encrypt password 
                    String encrypted_password = support.encrypt_utils.encrypt(context_dir, db_password);
                    //update return_linked_has_map
                    source_linked_has_map.replace("db_password",encrypted_password); 
                    source_linked_has_map.replace("db_password_encrypted","true");
                }
                if(email_password_encrypted.equalsIgnoreCase("false"))
                {
                    System.out.println("line 57");
                    needs_file_rewrite = true;
                    //encrypt password 
                    String encrypted_password = support.encrypt_utils.encrypt(context_dir, email_password);
                    //update return_linked_has_map
                    source_linked_has_map.replace("email_password",encrypted_password); 
                    source_linked_has_map.replace("email_password_encrypted","true");
                }
            }
            else
            {
                logger.error("in config.get_config: Can't find " + config_file.getAbsolutePath());
            }
        }
        catch (Exception exc)
        {
            logger.error("in config.get_config:" +  exc);
        } 
        if(needs_file_rewrite)
        {
            boolean success = properties_util.save.linked_hash_map(config_file, source_linked_has_map);
        }
        //logger.debug("End config.get_config");
        return source_linked_has_map;
    }
    public static Date get_config_file_date()
    {
        java.util.Date return_date = null;
        logger.debug("Start config.get_config_file_date");
        File config_file = new File("integration.properties");
        try
        {
            if (config_file.exists()) 
            {
                return_date = new Date(config_file.lastModified());
                //System.out.println("integration.properties last modified date = "+ current_properties_file_date.getTime());
                //System.out.println(last_properties_file_date.getTime());
                // System.out.println(current_properties_file_date.getTime());
            }
            else
            {
                logger.error("in config.get_config_file_date: Can't find " + config_file.getAbsolutePath());
            }
        }
        catch (Exception exc)
        {
            logger.error("in config.get_config_file_date:" +  exc);
        } 
        logger.debug("End config.get_config_file_date");
        return return_date;
    }
}
