//Copyright 2020 XaSystems, Inc. , All rights reserved.
package support;


import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.security.Key;
import javax.crypto.Cipher;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * TODO: Document me!
 *
 * @author XaSystems, Inc
 */
public final class encrypt_utils 
{
    private static final Logger logger = LogManager.getLogger(encrypt_utils.class.getName());
    
    public static String encrypt(String context_dir, String value) 
    {
        try (InputStream is = new FileInputStream(context_dir + "/WEB-INF/serveoptics.ser");
                ObjectInputStream ois = new ObjectInputStream(is)) 
        {
            Key key = (Key) ois.readObject();

            assert key != null : "Key for encryption not found";

            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] crypted = cipher.doFinal(value.getBytes("UTF8"));            
            byte[] encodedBytes = Base64.getEncoder().encode(crypted);            
            return new String(encodedBytes);

        } catch (Exception e) 
        {
            logger.error("ERROR in encrypt_utils.encrypt:=" + e);
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(String context_dir, String value) 
    {
        try (InputStream is = new FileInputStream(context_dir + "/WEB-INF/serveoptics.ser");
                ObjectInputStream ois = new ObjectInputStream(is)) 
        {
            Key key = (Key) ois.readObject();

            assert key != null : "Key for encryption not found";

            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);

            //byte[] decoded = Base64.decodeBase64(value);
            byte[] decoded = Base64.getDecoder().decode(value);
            byte[] decrypted = cipher.doFinal(decoded);
            return new String(decrypted, "UTF8");

        } 
        catch (Exception e) 
        {
            logger.error("ERROR in encrypt_utils.decrypt:=" + e);
            
            return "";
        }
    }
    
    public static JsonArray decrypt_jsonarray(String context_dir, JsonArray json_array)
    {
        JsonArrayBuilder unencrypted_users = Json.createArrayBuilder();    
        try (InputStream is = new FileInputStream(context_dir + "/WEB-INF/serveoptics.ser");
                ObjectInputStream ois = new ObjectInputStream(is)) 
        {
            Key key = (Key) ois.readObject();
            assert key != null : "Key for encryption not found";
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            JsonArray encrypted_users = json_array;
            
            for(int a = 0; a < encrypted_users.size();a++)
            {
                JsonObject this_record = encrypted_users.getJsonObject(a); //a single user record
                
                JsonObject user_record = Json.createObjectBuilder()
                .add("action", new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("action"))), "UTF8"))
                .add("contract_id", new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("contract_id"))), "UTF8"))
                .add("id", new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("id"))), "UTF8"))
                .add("username",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("username"))), "UTF8"))
                .add("password",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("password"))), "UTF8"))
                .add("first",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("first"))), "UTF8"))
                .add("mi",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("mi"))), "UTF8"))
                .add("last", new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("last"))), "UTF8"))
                .add("address_1",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("address_1"))), "UTF8"))
                .add("address_2",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("address_2"))), "UTF8"))
                .add("city",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("city"))), "UTF8"))
                .add("state",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("state"))), "UTF8"))
                .add("zip",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("zip"))), "UTF8"))
                .add("email",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("email"))), "UTF8"))
                .add("phone_office",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("phone_office"))), "UTF8"))
                .add("phone_mobile",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("phone_mobile"))), "UTF8"))
                .add("notes",new String(cipher.doFinal(Base64.getDecoder().decode(this_record.getString("notes"))), "UTF8"))
                .build(); 
                unencrypted_users.add(user_record);
            }
        } 
        catch (Exception e) 
        {
            logger.error("ERROR in encrypt_utils.decrypt_jsonarray:=" + e);
        }
        return unencrypted_users.build();
    }
}
