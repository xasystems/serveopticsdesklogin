//Copyright 2020 XaSystems, Inc. , All rights reserved.
package db;

import java.sql.*;
import java.util.LinkedHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import support.encrypt_utils;


public class db_util extends Thread 
{
    private static Logger logger = LogManager.getLogger();
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static synchronized void take_rest(long time) 
    {
        try 
        {
            Thread.currentThread().sleep(time);
        }
        catch (InterruptedException ey) 
        {
            logger.error("ERROR in db_util.take_rest:=" + ey);
            ey.printStackTrace();
        }
    }
    public static Connection get_connection(String context_dir, LinkedHashMap serveoptics_properties)
    {
        Connection con = null;
        String db = serveoptics_properties.get("db").toString();
        String db_host = serveoptics_properties.get("db_host").toString();
        String db_port = serveoptics_properties.get("db_port").toString();
        String db_username = serveoptics_properties.get("db_user").toString();
        String db_password = serveoptics_properties.get("db_password").toString();
        String db_password_encrypted = serveoptics_properties.get("db_password_encrypted").toString();
        String debug = serveoptics_properties.get("debug").toString();
        String log_level = serveoptics_properties.get("log_level").toString();
         
        if(db_password_encrypted.equalsIgnoreCase("true"))
        {
            db_password = encrypt_utils.decrypt(context_dir, db_password);
        }
        
        int trys = 0;
        while(trys < 9 && con == null)
        {
            String url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db.toLowerCase() + "?useTimezone=true&serverTimezone=UTC&characterEncoding=utf8&useSSL=false&useServerPrepStmts=false&autoReconnect=true";
            try
            {
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance(); 
            }
            catch(ClassNotFoundException e)
            {
                trys++;
                System.out.print("Can't find the database driver.  ClassNotFoundException: ");
                System.out.println(e.getMessage());
                take_rest(1000);
            }
            catch(Exception ep)
            {
                trys++;
                System.out.println("General Exception:=" + ep);
                take_rest(1000);
            }
            
            try
            {
                //System.out.println("url=" + url + " user=" + db_username + " password=" + db_password);
                con = DriverManager.getConnection(url, db_username, db_password);                
            }
            catch(SQLException ex)
            {
                trys++;
                System.out.println("==> SQLException: In DeskLogin db_util.get_connection: URL=" + url);
                while(ex != null) 
                {
                    System.out.println("Message:   " + ex.getMessage());
                    System.out.println("SQLState:  " + ex.getSQLState());
                    System.out.println("ErrorCode: " + ex.getErrorCode());
                    ex = ex.getNextException();
                    System.out.println("");
                }
                take_rest(1000);
            }
        }//End while  
        return con;
    }
}