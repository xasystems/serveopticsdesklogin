//Copyright 2020 XaSystems, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_contract 
{
    private static Logger logger = LogManager.getLogger();
    public static HashMap by_id(Connection con, String id) throws IOException, SQLException
    {
        HashMap return_hashmap = null;         
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM contracts WHERE id=?");
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap = new HashMap();
                return_hashmap.put("id",rs.getString("id"));//`id` int NOT NULL,
                return_hashmap.put("name",rs.getString("name"));//`id` int NOT NULL,
                return_hashmap.put("license",rs.getString("license"));//`license` text,
                return_hashmap.put("url",rs.getString("url"));//`url` varchar(255) DEFAULT NULL,
                return_hashmap.put("so_instance_id",rs.getString("so_instance_id"));//`so_instance_id` varchar(45) DEFAULT NULL,
                return_hashmap.put("poc_user_id",rs.getString("poc_user_id"));//`poc_user_id` int DEFAULT NULL,
                return_hashmap.put("active", rs.getString("active"));//`active` varchar45,
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_contract.by_id:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_contract.by_id:=" + exc);
	}
        return return_hashmap;
    }    
        
}
