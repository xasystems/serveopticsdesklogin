//Copyright 2020 Real Data Technologies, Inc. , All rights reserved.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author server-xc6701
 */
public class get_users 
{
    private static Logger logger = LogManager.getLogger();    
    
    public static HashMap by_username(Connection con, String username) throws IOException, SQLException
    {
        HashMap return_hashmap = null;         
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE username=?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap = new HashMap();
                return_hashmap.put("id",rs.getString("id")); //`id` int NOT NULL AUTO_INCREMENT,
                return_hashmap.put("contract_id",rs.getString("contract_id")); //`contract_id` int DEFAULT NULL,
                return_hashmap.put("username",rs.getString("username")); //`username` varchar(255) DEFAULT NULL,
                return_hashmap.put("password",rs.getString("password")); //`password` varchar(255) DEFAULT NULL,
                return_hashmap.put("first",rs.getString("first")); //`first` varchar(45) DEFAULT NULL,
                return_hashmap.put("mi",rs.getString("mi")); //`mi` varchar(45) DEFAULT NULL,
                return_hashmap.put("last",rs.getString("last")); //`last` varchar(255) DEFAULT NULL,
                return_hashmap.put("address_1",rs.getString("address_1")); //`address1` varchar(255) DEFAULT NULL,
                return_hashmap.put("address_2",rs.getString("address_2")); //`address2` varchar(255) DEFAULT NULL,
                return_hashmap.put("city",rs.getString("city")); //`city` varchar(255) DEFAULT NULL,
                return_hashmap.put("state",rs.getString("state")); //`state` varchar(45) DEFAULT NULL,
                return_hashmap.put("zip",rs.getString("zip")); //`zip` varchar(45) DEFAULT NULL,
                return_hashmap.put("email",rs.getString("email")); //`email` varchar(255) DEFAULT NULL,
                return_hashmap.put("phone_office",rs.getString("phone_office")); //`phone` varchar(45) DEFAULT NULL,
                return_hashmap.put("phone_mobile",rs.getString("phone_mobile")); //`mobile` varchar(45) DEFAULT NULL,
                return_hashmap.put("notes",rs.getString("notes")); //`notes` varchar(45) DEFAULT NULL,
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_username:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_username:=" + exc);
	}
        return return_hashmap;
    }    
    public static HashMap by_email(Connection con, String email) throws IOException, SQLException
    {
        HashMap return_hashmap = null;         
        PreparedStatement stmt;
        try 
        {
            // Create a Statement Object
            stmt = con.prepareStatement("SELECT * FROM users WHERE email=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                return_hashmap = new HashMap();
                return_hashmap.put("id",rs.getString("id")); //`id` int NOT NULL AUTO_INCREMENT,
                return_hashmap.put("contract_id",rs.getString("contract_id")); //`contract_id` int DEFAULT NULL,
                return_hashmap.put("username",rs.getString("username")); //`username` varchar(255) DEFAULT NULL,
                return_hashmap.put("password",rs.getString("password")); //`password` varchar(255) DEFAULT NULL,
                return_hashmap.put("first",rs.getString("first")); //`first` varchar(45) DEFAULT NULL,
                return_hashmap.put("mi",rs.getString("mi")); //`mi` varchar(45) DEFAULT NULL,
                return_hashmap.put("last",rs.getString("last")); //`last` varchar(255) DEFAULT NULL,
                return_hashmap.put("address_1",rs.getString("address_1")); //`address1` varchar(255) DEFAULT NULL,
                return_hashmap.put("address_2",rs.getString("address_2")); //`address2` varchar(255) DEFAULT NULL,
                return_hashmap.put("city",rs.getString("city")); //`city` varchar(255) DEFAULT NULL,
                return_hashmap.put("state",rs.getString("state")); //`state` varchar(45) DEFAULT NULL,
                return_hashmap.put("zip",rs.getString("zip")); //`zip` varchar(45) DEFAULT NULL,
                return_hashmap.put("email",rs.getString("email")); //`email` varchar(255) DEFAULT NULL,
                return_hashmap.put("phone_office",rs.getString("phone_office")); //`phone` varchar(45) DEFAULT NULL,
                return_hashmap.put("phone_mobile",rs.getString("phone_mobile")); //`mobile` varchar(45) DEFAULT NULL,
                return_hashmap.put("notes",rs.getString("notes")); //`notes` varchar(45) DEFAULT NULL,
            }
            stmt.close();
        }
        catch(SQLException ex) 
        {
            logger.error("ERROR SQLException get_users.by_email:=" + ex);
        }
        catch(Exception exc) 
        {
            logger.error("ERROR Exception get_users.by_email:=" + exc);
	}
        return return_hashmap;
    }    
            
}
